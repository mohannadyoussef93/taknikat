library serializer;

import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_collection/built_collection.dart';
import 'package:taknikat/model/base_response/base_response.dart';
import 'package:taknikat/model/category_model/category_model.dart';

part 'serializer.g.dart';

@SerializersFor(const [
  BaseResponse,
  CategoryModel,
])
final Serializers serializers = (_$serializers.toBuilder()
      ..addPlugin(StandardJsonPlugin())
      ..addBuilderFactory(
          (FullType(
            BaseResponse,
          )),
          () => BaseResponseBuilder<BaseResponse>())
      ..addBuilderFactory(
          (FullType(
            BaseResponse,
            [
              FullType(
                BuiltList,
                [
                  const FullType(CategoryModel),
                ],
              ),
            ],
          )),
          () => BaseResponseBuilder<BuiltList<CategoryModel>>()))
    .build();
