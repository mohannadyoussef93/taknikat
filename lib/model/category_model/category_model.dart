library category_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:taknikat/model/serializer/serializer.dart';

part 'category_model.g.dart';

abstract class CategoryModel implements Built<CategoryModel, CategoryModelBuilder> {

  // fields go here

  int get id;

  String get image;

  String get slug;

  @BuiltValueField(wireName: "category_id")
  String get categoryId;

  String get status;

  String get type;

  String get name;

  String get description;



  @nullable
  BuiltList<CategoryModel> get categories;

  CategoryModel._();

  factory CategoryModel([updates(CategoryModelBuilder b)]) = _$CategoryModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CategoryModel.serializer, this));
  }

  static CategoryModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CategoryModel.serializer, json.decode(jsonString));
  }

  static Serializer<CategoryModel> get serializer => _$categoryModelSerializer;
}
