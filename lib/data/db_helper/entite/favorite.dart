import 'package:built_value/built_value.dart';
import 'package:floor/floor.dart';

@entity
class Favorite {
  @PrimaryKey(autoGenerate: true)
  int id;

  String title ;
  String department;
  String image;
  String city;
  String country;
  String item_id;
  String type;

  Favorite({this.id, this.title, this.department, this.image, this.city, this.country, this.item_id,this.type});
}
