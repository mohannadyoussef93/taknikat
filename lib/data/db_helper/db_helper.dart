import 'package:taknikat/data/db_helper/db/database.dart';

import 'Idb_helper.dart';

class DbHelper implements IDbHelper {
  @override
  Future<AppDatabase> _getInstDB() async {
    return await $FloorAppDatabase.databaseBuilder('TaxiAppDriver.db').build();
  }
}
