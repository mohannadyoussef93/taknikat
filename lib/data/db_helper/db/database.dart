import 'dart:async';
import 'package:floor/floor.dart';

import 'package:path/path.dart';

import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:taknikat/data/db_helper/dao/cart_dao.dart';
import 'package:taknikat/data/db_helper/dao/favorite_dao.dart';
import 'package:taknikat/data/db_helper/entite/cart.dart';
import 'package:taknikat/data/db_helper/entite/favorite.dart';

part 'database.g.dart'; // the generated code will be there

@Database(version: 2, entities: [Favorite, Cart])
abstract class AppDatabase extends FloorDatabase {
  FavoriteDao get favoriteDao;
  CartDao get cartDao;
}
