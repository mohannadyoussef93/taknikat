// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String name;

  final List<Migration> _migrations = [];

  Callback _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String> listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  FavoriteDao _favoriteDaoInstance;

  CartDao _cartDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 2,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Favorite` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `title` TEXT, `department` TEXT, `image` TEXT, `city` TEXT, `country` TEXT, `item_id` TEXT, `type` TEXT)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Cart` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `title` TEXT, `item_id` TEXT, `image` TEXT, `quantity` INTEGER, `price` REAL)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  FavoriteDao get favoriteDao {
    return _favoriteDaoInstance ??= _$FavoriteDao(database, changeListener);
  }

  @override
  CartDao get cartDao {
    return _cartDaoInstance ??= _$CartDao(database, changeListener);
  }
}

class _$FavoriteDao extends FavoriteDao {
  _$FavoriteDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _favoriteInsertionAdapter = InsertionAdapter(
            database,
            'Favorite',
            (Favorite item) => <String, dynamic>{
                  'id': item.id,
                  'title': item.title,
                  'department': item.department,
                  'image': item.image,
                  'city': item.city,
                  'country': item.country,
                  'item_id': item.item_id,
                  'type': item.type
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _favoriteMapper = (Map<String, dynamic> row) => Favorite(
      id: row['id'] as int,
      title: row['title'] as String,
      department: row['department'] as String,
      image: row['image'] as String,
      city: row['city'] as String,
      country: row['country'] as String,
      item_id: row['item_id'] as String,
      type: row['type'] as String);

  final InsertionAdapter<Favorite> _favoriteInsertionAdapter;

  @override
  Future<List<Favorite>> getFavorite() async {
    return _queryAdapter.queryList('SELECT * FROM Favorite',
        mapper: _favoriteMapper);
  }

  @override
  Future<void> deleteFavorite(int id) async {
    await _queryAdapter.queryNoReturn('DELETE FROM Favorite where id = ?',
        arguments: <dynamic>[id]);
  }

  @override
  Future<void> insertFavorite(Favorite favorite) async {
    await _favoriteInsertionAdapter.insert(favorite, OnConflictStrategy.abort);
  }
}

class _$CartDao extends CartDao {
  _$CartDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _cartInsertionAdapter = InsertionAdapter(
            database,
            'Cart',
            (Cart item) => <String, dynamic>{
                  'id': item.id,
                  'title': item.title,
                  'item_id': item.item_id,
                  'image': item.image,
                  'quantity': item.quantity,
                  'price': item.price
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _cartMapper = (Map<String, dynamic> row) => Cart(
      id: row['id'] as int,
      title: row['title'] as String,
      item_id: row['item_id'] as String,
      image: row['image'] as String,
      quantity: row['quantity'] as int,
      price: row['price'] as double);

  final InsertionAdapter<Cart> _cartInsertionAdapter;

  @override
  Future<List<Cart>> getCart() async {
    return _queryAdapter.queryList('SELECT * FROM Cart', mapper: _cartMapper);
  }

  @override
  Future<void> deleteCart(int id) async {
    await _queryAdapter.queryNoReturn('DELETE FROM Cart where id = ?',
        arguments: <dynamic>[id]);
  }

  @override
  Future<void> insertCart(Cart cart) async {
    await _cartInsertionAdapter.insert(cart, OnConflictStrategy.abort);
  }
}
