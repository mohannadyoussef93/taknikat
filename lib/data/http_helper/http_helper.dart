import 'dart:convert';
import 'dart:io';

import 'package:built_value/serializer.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:taknikat/core/error/error.dart';
import 'package:taknikat/model/base_response/base_response.dart';
import 'package:taknikat/model/category_model/category_model.dart';
import 'package:taknikat/model/serializer/serializer.dart';

import 'Ihttp_helper.dart';

import 'package:cookie_jar/cookie_jar.dart';
import 'package:built_collection/built_collection.dart';

class HttpHelper implements IHttpHelper {
  final Dio _dio;

  var cookieJar = CookieJar();

  HttpHelper(this._dio) {
    _dio.interceptors.add(
      LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
      ),
    );
    _dio.interceptors.add(CookieManager(cookieJar));
  }

  @override
  Future<BuiltList<CategoryModel>> getCategory() async {
    _dio.interceptors.add(CookieManager(cookieJar));
    try {

      final response = await _dio.get('categories?type=services');
      print('getCategory Response StatusCode ${response.statusCode}');
      if (response.statusCode == 200) {
        print('getCategory Response body  ${response.data}');

        final BaseResponse<BuiltList<CategoryModel>> baseResponse =
        serializers.deserialize(json.decode(response.data),
            specifiedType: FullType(
              BaseResponse,
              [
                FullType(
                  BuiltList,
                  [
                    const FullType(CategoryModel),
                  ],
                ),
              ],
            ));


        if (baseResponse.message != null) {
          return baseResponse.data;
        } else {
          throw NetworkException();
        }
      } else {
        throw NetworkException();
      }
    } catch (e) {
      print(e.toString());
      throw NetworkException();
    }
  }

}
