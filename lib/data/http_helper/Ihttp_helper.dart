
import 'dart:io';



import 'package:taknikat/model/category_model/category_model.dart';

import '../db_helper/entite/cart.dart';
import 'package:built_collection/built_collection.dart';

abstract class IHttpHelper {
  Future<BuiltList<CategoryModel>> getCategory();
}
