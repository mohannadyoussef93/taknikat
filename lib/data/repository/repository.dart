import 'dart:ffi';
import 'dart:io';
import 'dart:io';

import 'package:built_collection/src/list.dart';

import 'package:taknikat/data/db_helper/Idb_helper.dart';
import 'package:taknikat/data/http_helper/Ihttp_helper.dart';
import 'package:taknikat/data/prefs_helper/iprefs_helper.dart';
import 'package:taknikat/model/category_model/category_model.dart';
import 'irepository.dart';

class Repository implements IRepository {
  IHttpHelper _ihttpHelper;
  IPrefsHelper _iprefHelper;

  IDbHelper _iDbHelper;

  Repository(this._ihttpHelper, this._iprefHelper, this._iDbHelper);



  @override
  Future<int> getAppLanguage() async {
    return await _iprefHelper.getAppLanguage();
  }


  @override
  Future<void> setAppLanguage(int value) async {
    await _iprefHelper.setAppLanguage(value);
  }

  @override
  Future<BuiltList<CategoryModel>> getCategory() async {

    return await _ihttpHelper.getCategory();
  }

}
