import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:taknikat/model/category_model/category_model.dart';



abstract class IRepository {
  Future<int> getAppLanguage();
  Future<void> setAppLanguage(int value);

  Future<BuiltList<CategoryModel>> getCategory();
}
