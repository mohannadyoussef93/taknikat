
import 'package:bloc/bloc.dart';
import 'package:taknikat/data/repository/irepository.dart';

import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  int total = 0;
  int totalOffers = 0;
  IRepository _repository;

  HomeBloc(this._repository);

  int currentPage = 1;
  int totalPage = 0;

  int currentPageStores = 1;
  int totalPageStores = 0;

  @override
  HomeState get initialState => HomeState.initail();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }

    if (event is GetCategory) {
      try {
        print("categoryEvent1");
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..category.replace([]));

        final data = await _repository.getCategory();
        print('GetCategory Success data ${data}');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..category.replace(data));
      } catch (e) {
        print('GetCategory Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong"
          ..category.replace([]));
      }
    }

  }
}
