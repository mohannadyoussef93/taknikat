// GENERATED CODE - DO NOT MODIFY BY HAND

part of home_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetStores extends GetStores {
  factory _$GetStores([void Function(GetStoresBuilder) updates]) =>
      (new GetStoresBuilder()..update(updates)).build();

  _$GetStores._() : super._();

  @override
  GetStores rebuild(void Function(GetStoresBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetStoresBuilder toBuilder() => new GetStoresBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetStores;
  }

  @override
  int get hashCode {
    return 594713033;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetStores').toString();
  }
}

class GetStoresBuilder implements Builder<GetStores, GetStoresBuilder> {
  _$GetStores _$v;

  GetStoresBuilder();

  @override
  void replace(GetStores other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetStores;
  }

  @override
  void update(void Function(GetStoresBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetStores build() {
    final _$result = _$v ?? new _$GetStores._();
    replace(_$result);
    return _$result;
  }
}

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$GetFeaturedStores extends GetFeaturedStores {
  factory _$GetFeaturedStores(
          [void Function(GetFeaturedStoresBuilder) updates]) =>
      (new GetFeaturedStoresBuilder()..update(updates)).build();

  _$GetFeaturedStores._() : super._();

  @override
  GetFeaturedStores rebuild(void Function(GetFeaturedStoresBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetFeaturedStoresBuilder toBuilder() =>
      new GetFeaturedStoresBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetFeaturedStores;
  }

  @override
  int get hashCode {
    return 284920779;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetFeaturedStores').toString();
  }
}

class GetFeaturedStoresBuilder
    implements Builder<GetFeaturedStores, GetFeaturedStoresBuilder> {
  _$GetFeaturedStores _$v;

  GetFeaturedStoresBuilder();

  @override
  void replace(GetFeaturedStores other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetFeaturedStores;
  }

  @override
  void update(void Function(GetFeaturedStoresBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetFeaturedStores build() {
    final _$result = _$v ?? new _$GetFeaturedStores._();
    replace(_$result);
    return _$result;
  }
}

class _$GetCategory extends GetCategory {
  factory _$GetCategory([void Function(GetCategoryBuilder) updates]) =>
      (new GetCategoryBuilder()..update(updates)).build();

  _$GetCategory._() : super._();

  @override
  GetCategory rebuild(void Function(GetCategoryBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetCategoryBuilder toBuilder() => new GetCategoryBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetCategory;
  }

  @override
  int get hashCode {
    return 577871736;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetCategory').toString();
  }
}

class GetCategoryBuilder implements Builder<GetCategory, GetCategoryBuilder> {
  _$GetCategory _$v;

  GetCategoryBuilder();

  @override
  void replace(GetCategory other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetCategory;
  }

  @override
  void update(void Function(GetCategoryBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetCategory build() {
    final _$result = _$v ?? new _$GetCategory._();
    replace(_$result);
    return _$result;
  }
}

class _$GetSlider extends GetSlider {
  factory _$GetSlider([void Function(GetSliderBuilder) updates]) =>
      (new GetSliderBuilder()..update(updates)).build();

  _$GetSlider._() : super._();

  @override
  GetSlider rebuild(void Function(GetSliderBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetSliderBuilder toBuilder() => new GetSliderBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetSlider;
  }

  @override
  int get hashCode {
    return 344133603;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetSlider').toString();
  }
}

class GetSliderBuilder implements Builder<GetSlider, GetSliderBuilder> {
  _$GetSlider _$v;

  GetSliderBuilder();

  @override
  void replace(GetSlider other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetSlider;
  }

  @override
  void update(void Function(GetSliderBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetSlider build() {
    final _$result = _$v ?? new _$GetSlider._();
    replace(_$result);
    return _$result;
  }
}

class _$Logout extends Logout {
  factory _$Logout([void Function(LogoutBuilder) updates]) =>
      (new LogoutBuilder()..update(updates)).build();

  _$Logout._() : super._();

  @override
  Logout rebuild(void Function(LogoutBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LogoutBuilder toBuilder() => new LogoutBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Logout;
  }

  @override
  int get hashCode {
    return 676553867;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('Logout').toString();
  }
}

class LogoutBuilder implements Builder<Logout, LogoutBuilder> {
  _$Logout _$v;

  LogoutBuilder();

  @override
  void replace(Logout other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Logout;
  }

  @override
  void update(void Function(LogoutBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Logout build() {
    final _$result = _$v ?? new _$Logout._();
    replace(_$result);
    return _$result;
  }
}

class _$GetPosts extends GetPosts {
  factory _$GetPosts([void Function(GetPostsBuilder) updates]) =>
      (new GetPostsBuilder()..update(updates)).build();

  _$GetPosts._() : super._();

  @override
  GetPosts rebuild(void Function(GetPostsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPostsBuilder toBuilder() => new GetPostsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPosts;
  }

  @override
  int get hashCode {
    return 621146292;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetPosts').toString();
  }
}

class GetPostsBuilder implements Builder<GetPosts, GetPostsBuilder> {
  _$GetPosts _$v;

  GetPostsBuilder();

  @override
  void replace(GetPosts other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetPosts;
  }

  @override
  void update(void Function(GetPostsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetPosts build() {
    final _$result = _$v ?? new _$GetPosts._();
    replace(_$result);
    return _$result;
  }
}

class _$GetNextPost extends GetNextPost {
  factory _$GetNextPost([void Function(GetNextPostBuilder) updates]) =>
      (new GetNextPostBuilder()..update(updates)).build();

  _$GetNextPost._() : super._();

  @override
  GetNextPost rebuild(void Function(GetNextPostBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetNextPostBuilder toBuilder() => new GetNextPostBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetNextPost;
  }

  @override
  int get hashCode {
    return 913876572;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetNextPost').toString();
  }
}

class GetNextPostBuilder implements Builder<GetNextPost, GetNextPostBuilder> {
  _$GetNextPost _$v;

  GetNextPostBuilder();

  @override
  void replace(GetNextPost other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetNextPost;
  }

  @override
  void update(void Function(GetNextPostBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetNextPost build() {
    final _$result = _$v ?? new _$GetNextPost._();
    replace(_$result);
    return _$result;
  }
}

class _$GetAboutUs extends GetAboutUs {
  factory _$GetAboutUs([void Function(GetAboutUsBuilder) updates]) =>
      (new GetAboutUsBuilder()..update(updates)).build();

  _$GetAboutUs._() : super._();

  @override
  GetAboutUs rebuild(void Function(GetAboutUsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetAboutUsBuilder toBuilder() => new GetAboutUsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetAboutUs;
  }

  @override
  int get hashCode {
    return 461827293;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetAboutUs').toString();
  }
}

class GetAboutUsBuilder implements Builder<GetAboutUs, GetAboutUsBuilder> {
  _$GetAboutUs _$v;

  GetAboutUsBuilder();

  @override
  void replace(GetAboutUs other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetAboutUs;
  }

  @override
  void update(void Function(GetAboutUsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetAboutUs build() {
    final _$result = _$v ?? new _$GetAboutUs._();
    replace(_$result);
    return _$result;
  }
}

class _$ContactUs extends ContactUs {
  @override
  final String name;
  @override
  final String text;

  factory _$ContactUs([void Function(ContactUsBuilder) updates]) =>
      (new ContactUsBuilder()..update(updates)).build();

  _$ContactUs._({this.name, this.text}) : super._() {
    if (name == null) {
      throw new BuiltValueNullFieldError('ContactUs', 'name');
    }
    if (text == null) {
      throw new BuiltValueNullFieldError('ContactUs', 'text');
    }
  }

  @override
  ContactUs rebuild(void Function(ContactUsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ContactUsBuilder toBuilder() => new ContactUsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ContactUs && name == other.name && text == other.text;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, name.hashCode), text.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ContactUs')
          ..add('name', name)
          ..add('text', text))
        .toString();
  }
}

class ContactUsBuilder implements Builder<ContactUs, ContactUsBuilder> {
  _$ContactUs _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _text;
  String get text => _$this._text;
  set text(String text) => _$this._text = text;

  ContactUsBuilder();

  ContactUsBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _text = _$v.text;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ContactUs other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ContactUs;
  }

  @override
  void update(void Function(ContactUsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ContactUs build() {
    final _$result = _$v ?? new _$ContactUs._(name: name, text: text);
    replace(_$result);
    return _$result;
  }
}

class _$MaintenanceEvent extends MaintenanceEvent {
  @override
  final List<File> images;
  @override
  final String phone;
  @override
  final String title;
  @override
  final String description;
  @override
  final String email;
  @override
  final String address;

  factory _$MaintenanceEvent(
          [void Function(MaintenanceEventBuilder) updates]) =>
      (new MaintenanceEventBuilder()..update(updates)).build();

  _$MaintenanceEvent._(
      {this.images,
      this.phone,
      this.title,
      this.description,
      this.email,
      this.address})
      : super._() {
    if (images == null) {
      throw new BuiltValueNullFieldError('MaintenanceEvent', 'images');
    }
    if (phone == null) {
      throw new BuiltValueNullFieldError('MaintenanceEvent', 'phone');
    }
    if (title == null) {
      throw new BuiltValueNullFieldError('MaintenanceEvent', 'title');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('MaintenanceEvent', 'description');
    }
    if (email == null) {
      throw new BuiltValueNullFieldError('MaintenanceEvent', 'email');
    }
    if (address == null) {
      throw new BuiltValueNullFieldError('MaintenanceEvent', 'address');
    }
  }

  @override
  MaintenanceEvent rebuild(void Function(MaintenanceEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MaintenanceEventBuilder toBuilder() =>
      new MaintenanceEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MaintenanceEvent &&
        images == other.images &&
        phone == other.phone &&
        title == other.title &&
        description == other.description &&
        email == other.email &&
        address == other.address;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, images.hashCode), phone.hashCode),
                    title.hashCode),
                description.hashCode),
            email.hashCode),
        address.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MaintenanceEvent')
          ..add('images', images)
          ..add('phone', phone)
          ..add('title', title)
          ..add('description', description)
          ..add('email', email)
          ..add('address', address))
        .toString();
  }
}

class MaintenanceEventBuilder
    implements Builder<MaintenanceEvent, MaintenanceEventBuilder> {
  _$MaintenanceEvent _$v;

  List<File> _images;
  List<File> get images => _$this._images;
  set images(List<File> images) => _$this._images = images;

  String _phone;
  String get phone => _$this._phone;
  set phone(String phone) => _$this._phone = phone;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _address;
  String get address => _$this._address;
  set address(String address) => _$this._address = address;

  MaintenanceEventBuilder();

  MaintenanceEventBuilder get _$this {
    if (_$v != null) {
      _images = _$v.images;
      _phone = _$v.phone;
      _title = _$v.title;
      _description = _$v.description;
      _email = _$v.email;
      _address = _$v.address;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MaintenanceEvent other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MaintenanceEvent;
  }

  @override
  void update(void Function(MaintenanceEventBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MaintenanceEvent build() {
    final _$result = _$v ??
        new _$MaintenanceEvent._(
            images: images,
            phone: phone,
            title: title,
            description: description,
            email: email,
            address: address);
    replace(_$result);
    return _$result;
  }
}

class _$GetNextStore extends GetNextStore {
  factory _$GetNextStore([void Function(GetNextStoreBuilder) updates]) =>
      (new GetNextStoreBuilder()..update(updates)).build();

  _$GetNextStore._() : super._();

  @override
  GetNextStore rebuild(void Function(GetNextStoreBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetNextStoreBuilder toBuilder() => new GetNextStoreBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetNextStore;
  }

  @override
  int get hashCode {
    return 146908065;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetNextStore').toString();
  }
}

class GetNextStoreBuilder
    implements Builder<GetNextStore, GetNextStoreBuilder> {
  _$GetNextStore _$v;

  GetNextStoreBuilder();

  @override
  void replace(GetNextStore other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetNextStore;
  }

  @override
  void update(void Function(GetNextStoreBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetNextStore build() {
    final _$result = _$v ?? new _$GetNextStore._();
    replace(_$result);
    return _$result;
  }
}

class _$GetOffers extends GetOffers {
  factory _$GetOffers([void Function(GetOffersBuilder) updates]) =>
      (new GetOffersBuilder()..update(updates)).build();

  _$GetOffers._() : super._();

  @override
  GetOffers rebuild(void Function(GetOffersBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetOffersBuilder toBuilder() => new GetOffersBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetOffers;
  }

  @override
  int get hashCode {
    return 883779373;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetOffers').toString();
  }
}

class GetOffersBuilder implements Builder<GetOffers, GetOffersBuilder> {
  _$GetOffers _$v;

  GetOffersBuilder();

  @override
  void replace(GetOffers other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetOffers;
  }

  @override
  void update(void Function(GetOffersBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetOffers build() {
    final _$result = _$v ?? new _$GetOffers._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
