library home_state;

import 'dart:convert';


import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:taknikat/model/category_model/category_model.dart';

part 'home_state.g.dart';

abstract class HomeState implements Built<HomeState, HomeStateBuilder> {
  // fields go here

  String get error;

  bool get isLoading;


  BuiltList<CategoryModel> get category;


  HomeState._();

  factory HomeState([updates(HomeStateBuilder b)]) = _$HomeState;

  factory HomeState.initail() {
    return HomeState((b) => b
      ..error = ""
      ..isLoading = false
      ..category.replace([])
      );
  }
}
