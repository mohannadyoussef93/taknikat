library home_event;

import 'dart:convert';
import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'home_event.g.dart';

abstract class HomeEvent {}

abstract class GetStores extends HomeEvent
    implements Built<GetStores, GetStoresBuilder> {
  // fields go here

  GetStores._();

  factory GetStores([updates(GetStoresBuilder b)]) = _$GetStores;
}

abstract class ClearError extends HomeEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}

abstract class GetFeaturedStores extends HomeEvent
    implements Built<GetFeaturedStores, GetFeaturedStoresBuilder> {
  // fields go here

  GetFeaturedStores._();

  factory GetFeaturedStores([updates(GetFeaturedStoresBuilder b)]) =
      _$GetFeaturedStores;
}

abstract class GetCategory extends HomeEvent
    implements Built<GetCategory, GetCategoryBuilder> {
  // fields go here

  GetCategory._();

  factory GetCategory([updates(GetCategoryBuilder b)]) = _$GetCategory;
}

abstract class GetSlider extends HomeEvent
    implements Built<GetSlider, GetSliderBuilder> {
  // fields go here

  GetSlider._();

  factory GetSlider([updates(GetSliderBuilder b)]) = _$GetSlider;
}

abstract class Logout extends HomeEvent
    implements Built<Logout, LogoutBuilder> {
  // fields go here

  Logout._();

  factory Logout([updates(LogoutBuilder b)]) = _$Logout;
}

abstract class GetPosts extends HomeEvent
    implements Built<GetPosts, GetPostsBuilder> {
  // fields go here

  GetPosts._();

  factory GetPosts([updates(GetPostsBuilder b)]) = _$GetPosts;
}

abstract class GetNextPost extends HomeEvent
    implements Built<GetNextPost, GetNextPostBuilder> {
  // fields go here

  GetNextPost._();

  factory GetNextPost([updates(GetNextPostBuilder b)]) = _$GetNextPost;
}

abstract class GetAboutUs extends HomeEvent
    implements Built<GetAboutUs, GetAboutUsBuilder> {
  // fields go here

  GetAboutUs._();

  factory GetAboutUs([updates(GetAboutUsBuilder b)]) = _$GetAboutUs;
}

abstract class ContactUs extends HomeEvent
    implements Built<ContactUs, ContactUsBuilder> {
  // fields go here

  String get name;

  String get text;

  ContactUs._();

  factory ContactUs([updates(ContactUsBuilder b)]) = _$ContactUs;
}

abstract class MaintenanceEvent extends HomeEvent
    implements Built<MaintenanceEvent, MaintenanceEventBuilder> {
  // fields go here

  List<File> get images;

  String get phone;

  String get title;

  String get description;

  String get email;

  String get address;

  MaintenanceEvent._();

  factory MaintenanceEvent([updates(MaintenanceEventBuilder b)]) =
      _$MaintenanceEvent;
}

abstract class GetNextStore extends HomeEvent
    implements Built<GetNextStore, GetNextStoreBuilder> {
  // fields go here

  GetNextStore._();

  factory GetNextStore([updates(GetNextStoreBuilder b)]) = _$GetNextStore;
}


abstract class GetOffers extends HomeEvent implements Built<GetOffers, GetOffersBuilder> {
  // fields go here

  GetOffers._();

  factory GetOffers([updates(GetOffersBuilder b)]) = _$GetOffers;
}