import 'dart:async';

import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:fluttertoast/fluttertoast.dart';

import '../../injectoin.dart';
import 'bloc/home_bloc.dart';
import 'bloc/home_event.dart';
import 'bloc/home_state.dart';
import 'package:taknikat/Ui/home_page/sub_category.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _bloc = sl<HomeBloc>();

  @override
  void initState() {
    _bloc.add(GetCategory());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: _bloc,
        builder: (BuildContext context, HomeState state) {
          error(state.error);
          return Scaffold(
              body: SingleChildScrollView(
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 500,
                      child: ListView.builder(
                          itemCount: state.category.length,
                          scrollDirection: Axis.vertical,
                          itemBuilder: (context, index) {
                            return Container(
                              child: Column(children: [
                                Container(
                                    height: 300,
                                    width: 300,
                                    child: Image.network(
                                        state.category[index].image)),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text("slug"),
                                    Text(state.category[index].slug),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text("status"),
                                    Text(state.category[index].status),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text("type"),
                                    Text(state.category[index].type),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text("name"),
                                    Text(state.category[index].name),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text("description"),
                                    Text(state.category[index].description),
                                  ],
                                ),
                                state.category[index].categories.isNotEmpty
                                    ? GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    SubCategory(
                                                      sub: state.category[index]
                                                          .categories,
                                                    )),
                                          );
                                        },
                                        child: Container(
                                          width: 300,
                                          height: 50,
                                          color: Colors.blue,
                                          child: Center(
                                              child: Text("sub Categories")),
                                        ))
                                    : Container()
                              ]),
                            );
                          }),
                    ),
                  ],
                ),
                state.isLoading
                    ? Center(
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.blue,
                        ),
                      )
                    : Container()
              ],
            ),
          ));
        });
  }

  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }
}
