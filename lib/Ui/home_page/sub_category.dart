import 'package:flutter/material.dart';
import 'package:taknikat/model/category_model/category_model.dart';
import 'package:built_collection/built_collection.dart';
class SubCategory extends StatefulWidget {

  BuiltList<CategoryModel> sub;

  SubCategory({this.sub});

  @override
  _SubCategoryState createState() => _SubCategoryState();
}

class _SubCategoryState extends State<SubCategory> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    height: 500,
                    child: ListView.builder(
                        itemCount: widget.sub.length,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, index) {
                          return Container(
                            child: Column(children: [
                              Container(
                                  height:300,
                                  width: 300,
                                  child: Image.network(widget.sub[index].image)),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text("slug"),
                                  Text(widget.sub[index].slug),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text("status"),
                                  Text(widget.sub[index].status),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text("type"),
                                  Text(widget.sub[index].type),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text("name"),
                                  Text(widget.sub[index].name),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text("description"),
                                  Text(widget.sub[index].description),
                                ],
                              ),

                              widget.sub[index].categories.isNotEmpty?
                              GestureDetector(
                                  onTap: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => SubCategory(sub: widget.sub[index].categories,)),
                                    );
                                  },
                                  child: Container(width: 300,height: 50,color: Colors.blue,child: Center(child: Text("sub Categories")),))
                                  :Container()
                            ]),
                          );
                        }),
                  ),

                ],
              ),
            ],
          ),
        ));
  }
}
