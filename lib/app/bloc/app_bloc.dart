import 'package:bloc/bloc.dart';
import 'package:taknikat/data/repository/irepository.dart';


import 'app_event.dart';
import 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  IRepository _repository;

  AppBloc(this._repository);

  @override
  AppState get initialState => AppState.initail();

  @override
  Stream<AppState> mapEventToState(
    AppEvent event,
  ) async* {
    if (event is IniEvent) {
      final language = await _repository.getAppLanguage();

      yield state.rebuild((b) => b
        ..appLanguage = language
        );

    }
  }
}
